/*jslint node: true */
var http = require('http'),
  querystring = require('querystring'),
  util = require('util'),

  readStaticFiles = require('./readFiles');

var ipaddress = process.env.OPENSHIFT_NODEJS_IP || null,
  port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 5000,
  maxDataLength = 1048576, // 1MiB
  appUrl;

var staticFiles = [{
  url: '/',
  name: 'res/info-page.html',
  options: 'utf-8',
  contentEncoding: 'utf-8',
  contentType: 'text/html'
}, {
  url: '/test',
  name: 'res/test-page.html',
  options: 'utf-8',
  contentEncoding: 'utf-8',
  contentType: 'text/html'
}, {
  url: '/favicon.ico',
  name: 'res/favicon.ico',
  contentType: 'image/x-icon'
}, {
  url: '/apple-touch-icon.png',
  name: 'res/apple-touch-icon.png',
  contentType: 'image/png'
}, {
  url: '/apple-touch-icon-precomposed.png',
  name: 'res/apple-touch-icon-precomposed.png',
  contentType: 'image/png'
}];

function dateUTC() {
  'use strict';
  return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}

function formatHost(address) {
  'use strict';
  return (address.family === 'IPv6' ? '[' + address.address + ']' : address.address) + ':' + address.port;
}

function getRemoteIP(request) {
  'use strict';
  return request.headers['x-forwarded-for'] || request.connection.remoteAddress || request.socket.remoteAddress || request.connection.socket.remoteAddress;
}

function logRequest(request, statusCode) {
  'use strict';
  var log;

  log = statusCode === 200 ? console.info : console.warn;
  log('%s: Request from %s: %s %s - %d: %s', dateUTC(), getRemoteIP(request), request.method, request.url, statusCode, http.STATUS_CODES[statusCode]);
}

function changeAppUrl(pageText) {
  'use strict';
  if (!appUrl) {
    if (process.env.OPENSHIFT_APP_DNS) { // OpenShift platform
      appUrl = 'http://' + process.env.OPENSHIFT_APP_DNS;
    } else if ((process.env.PATH || '').indexOf('heroku') > 0 || (process.env.NODE_HOME || '').indexOf('heroku') > 0) { // Heroku platform
      appUrl = 'https://datauridownload-eipdev.herokuapp.com';
    } else if ((process.env.PWD || '').indexOf('Download Service') > 0) { // localhost
      appUrl = 'http://localhost' + (port !== 80 ? ':' + port : '');
    } else {
      appUrl = undefined;
    }
  }
  if (appUrl) {
    return pageText.replace(/http:\/\/example\.com/g, appUrl);
  } else {
    return pageText;
  }
}

function errPageResponse(request, response, statusCode) {
  'use strict';
  var errPageTemplate = '<!DOCTYPE html><html><head><title>Error %d</title><body>Error %d: %s';
  response.writeHead(statusCode, {
    'Content-Type': 'text/html'
  });
  response.end(util.format(errPageTemplate, statusCode, statusCode, http.STATUS_CODES[statusCode]));
  logRequest(request, statusCode);
}

function sendResponse(request, response, data) {
  'use strict';

  data.contentLength = data.contentLength || (typeof data.content === 'string' ? Buffer.byteLength(data.content, data.contentEncoding) : data.content.length);
  logRequest(request, 200);
  response.writeHead(200, {
    'Content-Type': data.contentType,
    'Content-Length': data.contentLength
  });
  if (request.method === 'GET') {
    response.write(data.content);
    console.info('    %d bytes sent', data.contentLength);
  }
  response.end();
}

function processRequestData(data) {
  'use strict';
  var result, parts, headers, base64Enc;

  result = querystring.parse(data);
  if (!(result.data && result.data.indexOf('data:') === 0)) {
    return null;
  }
  parts = result.data.split(',');
  if (parts.length !== 2) {
    return null;
  }
  headers = parts[0];
  headers.split(';').forEach(function (header) {
    if (header.indexOf('data:') === 0) {
      result.mediatype = header.substr(5).trim();
    } else if (header.indexOf('charset=') === 0) {
      result.charset = header.substr(8).trim();
    } else if (header.indexOf('base64') === 0) {
      base64Enc = true;
    }
  });
  if (base64Enc) {
    result.data = new Buffer(parts[1], 'base64');
  } else {
    result.data = new Buffer(querystring.unescape(parts[1]), result.charset || 'utf-8');
  }
  return result;
}

function handleRequest(request, response) {
  'use strict';
  var found = false,
    requestData = '';

  response.on('error', function (err) {
    console.error('%s: Error %s', dateUTC(), err.message);
  });
  switch (request.method) {
  case 'HEAD':
  case 'GET':
    staticFiles.some(function (file) {
      if (request.url === file.url) {
        found = true;
        sendResponse(request, response, file);
      }
      return found;
    });
    if (!found) {
      errPageResponse(request, response, 404);
    }
    break;
  case 'POST':
    request.on('data', function (chunk) {
      // console.log('    %d bytes data chunk received', chunk.length);
      if (chunk.length <= maxDataLength && requestData.length + chunk.length <= maxDataLength) {
        requestData += chunk.toString();
      } else {
        requestData = undefined;
        errPageResponse(request, response, 413);
        request.socket.destroy();
      }
    });
    request.on('end', function () {
      var parsedData, headers;
      if (request.socket.destroyed) { //request was destroyed
        return;
      }
      parsedData = processRequestData(requestData);
      if (parsedData) {
        headers = {
          'Content-Disposition': 'attachment',
          'Content-Length': parsedData.data.length
        };
        if (parsedData.mediatype) {
          headers['Content-Type'] = parsedData.mediatype;
        }
        if (parsedData.charset) {
          headers['Content-Encoding'] = parsedData.charset;
        }
        response.writeHead(200, headers);
        response.write(parsedData.data);
        response.end();
        logRequest(request, 200);
        console.info('    %d bytes sent for download (request data size: %d bytes)', parsedData.data.length, requestData.length);
      } else {
        errPageResponse(request, response, 422);
      }
    });
    break;
  default:
    errPageResponse(request, response, 405);
  }
}

function initServer() {
  'use strict';
  var server, wasError;

  staticFiles.forEach(function (file) {
    if (file.error) {
      console.warn('%s: Error %s', dateUTC(), file.error.message);
      wasError = true;
    } else if (file.url === '/') {
      file.content = changeAppUrl(file.content);
    }
  });
  if (wasError) {
    console.error('%s: Failed initialization of static files. Exiting.', dateUTC());
    return;
  }
  server = http.createServer(handleRequest);

  server.listen(port, ipaddress, function () {
    console.info('%s: HTTP Server listening on: http://%s', dateUTC(), formatHost(this.address()));
  });
}

console.info('%s: %s %s [%s %s]: starting HTTP Server on "%s", Port: "%s"', dateUTC(), process.title, process.version, process.platform, process.arch, ipaddress, port);
readStaticFiles(staticFiles, initServer);
