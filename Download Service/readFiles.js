/*jslint node: true */
var fs = require('fs');

// Reads several files
// files: array of oblects {name: <file name>, options <read file options>}
// callback Function is passed two arguments: errors (array of objects), results (array of strings)
module.exports = function (files, callback) {
  'use strict';
  var count = 0;

  files.forEach(function (file, index) {
    fs.readFile(file.name, file.options, function (err, data) {
      if (err) {
        file.error = err;
      } else {
        file.content = data;
      }
      ++count;
      if (count === files.length) {
        callback(files);
      }
    });
  });
};