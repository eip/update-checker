/*jslint node: true */
var fs = require('fs');
var crypto = require('crypto');

function parseDataString(datastring) {
  'use strict';
  var curr, prev = 0,
    result = {},
    undefparamcnt = 1;

  function parseParam(paramstring) {
    var pos, name;

    if (!paramstring) {
      return;
    }
    pos = paramstring.indexOf('=');
    switch (pos) {
    case -1:
      name = paramstring.trim();
      break;
    case 0:
      name = '_undefined_' + undefparamcnt++;
      break;
    default:
      name = paramstring.substring(0, pos).trim();
    }
    result[name] = pos >= 0 ? paramstring.substring(pos + 1).trim() : '';

    // DEBUG
    if (result[name].length > 80) {
      result[name] = result[name].substr(0, 25) + ' ... ' + result[name].substr(result[name].length - 25) + ' MD5: ' + crypto.createHash('md5').update(result[name]).digest('hex');
    }

  }
  do {
    curr = datastring.indexOf('&', prev);
    parseParam(datastring.substring(prev, curr >= 0 ? curr : datastring.length));
    prev = curr + 1;
  } while (curr >= 0);
  return result;
}

var ds = fs.readFileSync('datauri-03.txt', 'utf-8');

console.log(parseDataString('text'));
console.log('');
console.log(parseDataString(ds));
console.log('');
console.log(parseDataString(ds + '&param3=value3'));
console.log('');
console.log(parseDataString('param1=value1&param2=value2&filename=sunflower.jpeg&' + ds));
console.log('');
console.log(parseDataString('param1=value1&param2=value2&filename=sunflower.jpeg&' + ds + '&param3=value3'));
console.log('');
console.log(parseDataString('param1=value1&=value2&filename=&' + ds + '&=value3&param4'));