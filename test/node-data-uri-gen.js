/*jslint node: true */
var fs = require('fs');

// var inFileName = 'sample-image-big-02.jpg',
var inFileName = 'sample-hello.txt',
  inFileType = 'text/plain;charset=UTF-8;',
  // inFileType = 'image/jpeg;',
  outFileName = inFileName + '.datauri.txt',
  inData = fs.readFileSync(inFileName);

var encoded = 'data:' + inFileType + 'base64,' + new Buffer(inData).toString('base64');
console.log(encoded);
encoded = 'data=' + encodeURIComponent(encoded);
fs.writeFileSync(outFileName, encoded);