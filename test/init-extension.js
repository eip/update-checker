/* global HTMLInspector: false */
(function main() {
  'use strict';

  function addStyle(css) {
    var style = document.createElement('style');

    style.type = 'text/css';
    style.innerText = css;
    document.head.appendChild(style);
  }

  function loadScript(scriptUrl, onScriptLoad) {
    var script = document.createElement('script');

    script.type = 'text/javascript';
    script.charset = 'utf-8';
    script.src = scriptUrl;
    if (onScriptLoad) {
      script.onload = onScriptLoad;
    }
    document.body.appendChild(script);
  }

  function ToolbarItem(badge, image, toolTip) {
    var b = badge,
      i = image,
      t = toolTip;

    Object.defineProperty(this, 'badge', {
      get: function _get() {
        return b;
      },
      set: function _set(value) {
        var elm;

        b = value;
        elm = document.getElementById('toolBarBadge');
        if (elm) {
          elm.innerText = b;
        }
      }
    });
    Object.defineProperty(this, 'image', {
      get: function _get() {
        return i;
      },
      set: function _set(value) {
        var elm;

        i = value;
        elm = document.getElementById('toolBarButton');
        if (!elm) {
          return;
        }
        elm.style.backgroundImage = 'url(' + i + ')';
        elm.style.backgroundImage = '-webkit-image-set(url(' + i + ') 1x, url(' + i.replace('.png', '@2x.png') + ') 2x)';
        elm.style.backgroundSize = 'auto 16px';
        elm.style.backgroundRepeat = 'no-repeat';
      }
    });
    Object.defineProperty(this, 'toolTip', {
      get: function _get() {
        return t;
      },
      set: function _set(value) {
        var elm;

        t = value;
        elm = document.getElementById('toolBarButton');
        if (elm) {
          elm.title = t;
        }
        // window.console.debug('ToolTip set to "' + t + '"');
      }
    });
    this.badge = b;
    this.image = i;
    this.toolTip = t;
  }

  window.safari = {
    application: {
      addEventListener: function _addEventListener() {
        window.console.debug('Calling safari.application.addEventListener()');
      },
      browserWindows: [{ tabs: [] }],
      activeBrowserWindow: {
        openTab: function _openTab() {
          var tab;

          window.console.debug('Calling safari.activeBrowserWindow.openTab()');
          tab = {
            activate: function _activate() {
              window.console.debug('Calling SafariBrowserTab.activate()');
            },
            browserWindow: {
              activate: function _activate() {
                window.console.debug('Calling SafariBrowserTab.browserWindow.activate()');
              }
            }
          };
          Object.defineProperty(tab, 'url', {
            set: function _set(s) {
              window.console.debug('SafariBrowserTab: Setting url to ' + s);
              window.open(s, '_blank');
            }
          });
          return tab;
        }
      }
    },
    self: {
      addEventListener: function _addEventListener(event, handler) {
        window.console.debug('Calling safari.self.addEventListener()');
        this.messageHandler = handler;
      },
      hide: function _hide() {
        window.console.debug('Calling safari.self.hide()');
      },
      tab: {
        dispatchMessage: function _dispatchMessage(message) {
          window.console.debug('Calling safari.self.tab.dispatchMessage()');
          if (message === 'getSettingsStorage') {
            window.safari.self.messageHandler({
              name: message,
              message: window.safari.extension.settings.settingsStorage
            });
          } else if (message === 'getCheckHistory') {
            window.safari.self.messageHandler({
              name: message,
              message: window.safari.extension.settings.checkHistory
            });
          }
        }
      }
    },
    extension: {
      baseURI: '../Update%20Checker.safariextension/',
      globalPage: {
        contentWindow: {
          lib: {
            cancelResetNewUpdates: function _cancelResetNewUpdates() {
              window.console.debug('Calling safari.extension.globalPage.contentWindow.lib.cancelResetNewUpdates()');
            },
            queryResetNewUpdates: function _queryResetNewUpdates() {
              window.console.debug('Calling safari.extension.globalPage.contentWindow.lib.queryResetNewUpdates()');
            }
          }
        }
      },
      toolbarItems: [new ToolbarItem(0, '', 'Update Checker')],
      settings: {
        addEventListener: function _addEventListener() {
          window.console.debug('Calling safari.application.addEventListener()');
        },
        openSettings: false,
        settingsStorage: {},
        checkHistory: {}
      }
    }
  };

  if (document.URL.indexOf('popover.html') >= 0) {
    addStyle('div.container { background: -webkit-linear-gradient(#FEFEFE, #ECECEC); width: 600px; height: 400px; overflow: auto; }');
    setTimeout(function _setTimeout() {
      window.lib.buildHistoryPage();
    }, 1);
  }

  loadScript('https://cdnjs.cloudflare.com/ajax/libs/html-inspector/0.8.2/html-inspector.js', function _onScriptLoad() { console.debug('HTML Inspector initialized.'); });
  setTimeout(function _setTimeout() { HTMLInspector.inspect(); }, 500);
}());
