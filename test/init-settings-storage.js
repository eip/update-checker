/* eslint quotes: "off" */
/* eslint quote-props: "off" */

'use strict';

// lib.getSettingsStorage(); setTimeout(function () { alert(JSON.stringify(settingsStorage, null, '  ')); }, 500);
window.safari.extension.settings.settingsStorage = {
  "checkInterval": 30,
  "logLevel": 0,
  "pages": [
    {
      "pageUrl": "http://boxeed.in/forums/viewtopic.php?f=13&t=148&sd=d",
      "pageTitle": "Official XBMC/KODI for Boxee Box",
      "checkUrl": "https://github.com/quasar1/boxeebox-xbmc/releases",
      "find": "/<h1\\sclass=\"release-title\">[\\s\\S]{1,50}<a.{10,80}>(kodi[-_]\\d{2,3}\\.\\d{1,3}[-_\\w]+)<\\/a>/i",
      "replace": "$1",
      "invalid": false
    },
    {
      "pageUrl": "http://www.redmine.org/projects/redmine/wiki/Download",
      "pageTitle": "Redmine: Latest releases",
      "checkUrl": "",
      "find": "/<a name=\"stable-releases\">[\\s\\S]+?<ul>[\\s]+?<li>(\\d{1,2}\\.\\d{1,2}\\.\\d{1,2})\\s\\((\\d{2,4})-(\\d{1,2})-(\\d{1,2})\\)/i",
      "replace": "Redmine - версия $1 от $4.$3.$2",
      "invalid": false
    },
    {
      "pageUrl": "https://bitnami.com/stack/redmine",
      "pageTitle": "Bitnami Redmine Stack",
      "checkUrl": "https://bitnami.com/stack/redmine/changelog.txt",
      "find": "/^version\\s([\\d\\.-]+)\\s+(\\d{2,4}).(\\d{1,2}).(\\d{1,2})$/im",
      "replace": "Bitnami Redmine - версия $1 от $4.$3.$2",
      "invalid": false
    },
    {
      "pageUrl": "http://1c.ru/rus/support/release/categ.jsp?GroupID=87&83",
      "pageTitle": "1С: Технологическая платформа 8.3",
      "checkUrl": "http://downloads.1c.ru/release_info/categ_js.jsp?GroupID=87&nohead=1",
      "find": "/<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(технологическая платформа)<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(8\\.3\\.\\d{1,2}\\.\\d{1,4})<\\//i",
      "replace": "$2 - версия $3 от $1",
      "invalid": false
    },
    {
      "pageUrl": "http://1c.ua/support/release.php?upp",
      "pageTitle": "1С: Конфигурация «УПП для Украины редакция 1.3»",
      "find": "/<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(управление производственным предприятием для украины,? редакция \\d{1,2}\\.\\d{1,2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{1,3}\\.\\d{1,3})<\\//i",
      "replace": "Конфигурация «$2» - версия $3 от $1",
      "checkUrl": "http://downloads.1c.ru/release_info/categ_js_ua.jsp?GroupID=97&amp;nohead=1",
      "invalid": false
    },
    {
      "pageUrl": "http://1c.ru/rus/support/release/categ.jsp?GroupID=88&erp2",
      "pageTitle": "1С: Конфигурация «ERP Управление предприятием 2.0»",
      "checkUrl": "http://downloads.1c.ru/release_info/categ_js.jsp?GroupID=88&nohead=1",
      "find": "/<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(erp управление предприятием \\d{1,2}\\.\\d{1,2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{1,3}\\.\\d{1,3})<\\//i",
      "replace": "Конфигурация «$2» - версия $3 от $1",
      "invalid": false
    },
    {
      "pageUrl": "http://1c.ru/rus/support/release/categ.jsp?GroupID=88&cnv",
      "pageTitle": "1С: Конфигурация «Конвертация данных»",
      "checkUrl": "http://downloads.1c.ru/release_info/categ_js.jsp?GroupID=88&nohead=1",
      "find": "/<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(конвертация данных)<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{1,3}\\.\\d{1,3})<\\//i",
      "replace": "Конфигурация «$2» - версия $3 от $1",
      "invalid": false
    },
    {
      "pageUrl": "http://habrahabr.ru/posts/collective",
      "pageTitle": "Хабрахабр: Лучшие публикации за сутки",
      "checkUrl": "",
      "find": "/<a href=\"https?:\\/\\/habrahabr.ru\\/[\\w\\/]+\\d+\\/?\"\\sclass=\\\"post_{1,2}title(?:_link)?\\\">(.+?)<\\/a>/i",
      "replace": "$1",
      "invalid": false
    },
    {
      "pageUrl": "https://f-droid.org/repository/browse/?fdid=org.adaway",
      "pageTitle": "F-Droid: AdAway",
      "checkUrl": "",
      "find": "/version\\s(\\d\\.\\d{1,2}(?:\\.\\d{1,2})?).{1,15}added\\son\\s(\\w{3})\\s(\\d{1,2}),\\s(\\d{2,4})/i",
      "replace": "AdAway - версия $1 от $2 $3, $4",
      "invalid": false
    },
    {
      "pageUrl": "http://4pda.ru/forum/index.php?showtopic=373995",
      "pageTitle": "4PDA: Listen Audiobook Player",
      "checkUrl": "",
      "find": "/скачать:.{1,50}версия:?\\s{1,3}(\\d{1,2}\\.\\d{1,2}\\.\\d{1,2})/i",
      "replace": "Listen Audiobook Player - версия $1",
      "invalid": false
    },
    {
      "pageUrl": "http://forums.mydigitallife.info/threads/50234-Emulated-KMS-Servers-on-non-Windows-platforms",
      "pageTitle": "VLMCSD (KMS Emulator)",
      "checkUrl": "https://forums.mydigitallife.net/threads/emulated-kms-servers-on-non-windows-platforms.50234",
      "find": "/changes.*[\\s\\S]+?<ul>\\s*<li.{0,10}>(\\d{2,4}).(\\d{1,2}).(\\d{1,2})\\s\\(.{1,5}?((?:svn)?\\d{2,5}).{1,5}\\)/i",
      "replace": "vlmcsd $4 от $3.$2.$1",
      "invalid": false
    },
    {
      "pageUrl": "http://twrp.me/devices/samsungnexus10.html",
      "pageTitle": "TWRP For Nexus 10",
      "checkUrl": "http://dl.twrp.me/manta",
      "find": "/<article\\sclass=\"post-content\">[\\s\\S]{1,50}<a.*?>twrp.(\\d{1,2}.\\d{1,2}.\\d{1,2}.\\d{1,2}).manta/i",
      "replace": "TWRP $1",
      "invalid": false
    },
    {
      "pageUrl": "http://www.fosshub.com/MKVToolNix.html",
      "pageTitle": "MKVToolNix",
      "checkUrl": "",
      "find": "/download[\\s\\S]{1,100}(mkvtoolnix).{1,20}(mac\\s?os(?:\\sx)?).*<\\/a>[\\s\\S]{1,300}version.\\s(\\d{1,2}.\\d{1,2}.\\d{1,2})/i",
      "replace": "$1 для $2 - версия $3",
      "invalid": false
    },
    {
      "pageUrl": "http://forum.xda-developers.com/android/software/tk-gapps-t3116347",
      "pageTitle": "TK Google Apps 5.1.x",
      "checkUrl": "[OLD]",
      "find": "/\\[gapps\\]\\[5\\..{1,5}].{1,50}tk\\sgapps.{1,5}\\[(\\d{2,4}).(\\d{1,2}).(\\d{1,2})\\]/i",
      "replace": "TK Google Apps от $3.$2.$1",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "https://bitnami.com/stack/prestashop",
      "pageTitle": "Bitnami Prestashop Stack",
      "checkUrl": "https://bitnami.com/stack/prestashop/changelog.txt",
      "find": "/^version\\s([\\d\\.-]+)\\s+(\\d{2,4}).(\\d{1,2}).(\\d{1,2})$/im",
      "replace": "Bitnami Prestashop - версия $1 от $4.$3.$2",
      "invalid": false
    },
    {
      "pageUrl": "http://www.apcupsd.org",
      "pageTitle": "apcupsd: A daemon for controlling APC UPSes",
      "checkUrl": "",
      "find": "/<article.*?>[\\S\\s]{1,200}<a.*?>apcupsd.(\\d{1,2}\\.\\d{1,3}\\.\\d{1,3}).released<\\/a>[\\S\\s]{1,3000}<footer.*?>\\s*this\\sentry\\swas\\sposted.*?datetime=\"(\\d{2,4}).(\\d{1,2}).(\\d{1,2})/i",
      "replace": "apcupsd $1 от $4.$3.$2",
      "invalid": false
    },
    {
      "pageUrl": "http://www.makemkv.com",
      "pageTitle": "MakeMKV",
      "checkUrl": "http://www.makemkv.com/download",
      "find": "/revision\\shistory<\\/\\w+>\\s+<li>(makemkv).*?(\\d{1,2}.\\d{1,2}.\\d{1,2})\\s*\\(\\s*(\\d{1,2}).(\\d{1,2}).(\\d{2,4})\\s*\\)<\\/li>/i",
      "replace": "$1 - версия $2 от $3.$4.$5",
      "invalid": false
    },
    {
      "pageUrl": "http://www.makemkv.com/forum2/viewtopic.php?f=5&t=1053",
      "pageTitle": "MakeMKV Beta Key",
      "checkUrl": "",
      "find": "/the\\scurrent\\sbeta\\skey.{1,200}<div\\sclass=['\"]codecontent['\"]>([\\w@-]+)<\\/div>.{1,40}(valid\\suntil.{10,40})\\./i",
      "replace": "Key: $1 - $2",
      "invalid": false
    },
    {
      "pageUrl": "http://xact.scottcbrown.org",
      "pageTitle": "xACT (X Audio Compression Toolkit)",
      "checkUrl": "",
      "find": "/version\\shistory[\\s\\S]{1,20}?(\\d{1,2}\\.\\d{1,3}\\s\\(build\\s\\d{2,5}\\)).{1,5}?(\\d{1,2})[\\.\\/](\\d{1,2})[\\.\\/](\\d{2,4})\\s/i",
      "replace": "xACT - версия $1 от $3.$2.$4",
      "invalid": false
    },
    {
      "pageUrl": "https://developers.google.com/android/nexus/images",
      "pageTitle": "Factory Images for Nexus 10",
      "checkUrl": "[OLD]",
      "find": "/<tr\\sid=\"mantaray\\w*\">\\s+<td>(\\d{1,2}\\.\\d{1,2}\\.\\d{1,2}\\s*\\(\\w+\\))[\\s\\S]{100,400}<\\/table>/",
      "replace": "Nexus 10: $1",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://help.getsync.com/hc/en-us",
      "pageTitle": "BitTorrent Sync for Synology NAS",
      "checkUrl": "[OLD]",
      "find": "/bittorrentsync_88f6281_(\\d{1,2}\\.\\d{1,2}\\.\\d{1,2})-\\d{1,2}\\.spk/",
      "replace": "BitTorrent Sync $1 (88f628x)",
      "invalid": {
        "find": "No matches with this regular expression",
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://devtool1c.ucoz.ru",
      "pageTitle": "Подсистема «Инструменты разработчика» 1С 8",
      "checkUrl": "http://devtool1c.ucoz.ru/load",
      "find": "/инструменты\\sразработчика\\sпортативные\\s1С\\s(8\\.\\d)\\sv(\\d{1,2}\\.\\d{1,3}[\\.\\d\\w]+)/i",
      "replace": "Портативные инструменты разработчика 1С $1 - версия $2",
      "invalid": false
    },
    {
      "pageUrl": "http://winebottler.kronenberg.org",
      "pageTitle": "WineBottler (Run Windows-based Programs on a Mac)",
      "checkUrl": "",
      "find": "/(winebottler)\\s*<span.*?>(\\d{1,2}.\\d{1,2}\\S*)\\s*(stable)/i",
      "replace": "$1 $2 $3",
      "invalid": false
    },
    {
      "pageUrl": "https://dl.twrp.me/manta",
      "pageTitle": "TWRP For Nexus 10",
      "checkUrl": "[OLD]",
      "find": "/<a.+?>(twrp-\\d.\\d.\\d.\\d-manta).+?<\\/a>/",
      "replace": "$1",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://4pda.ru/forum/index.php?showtopic=719753",
      "pageTitle": "Прошивка MIUI v7 для Xiaomi Redmi 3 от greatslon",
      "checkUrl": "http://4pda.ru/forum/index.php?showtopic=719753&st=1060#entry47194724",
      "find": "/<tr[^>]*>[\\S\\s]{1,4500}<td[^>]*>[\\S\\s]{1,360}представляю\\s+свою\\s+сборку\\s+MIUI[\\S\\s]{1,1600}версия\\s+от\\s+(\\d{1,2}\\.\\d{1,2}\\.\\d{2,4})[\\S\\s]{1,100}?([\\d\\.]{1,12}[a-z]*)/i",
      "replace": "версия от $1 на базе $2",
      "invalid": {
        "checkUrl": "Page content unavailable"
      }
    },
    {
      "pageUrl": "http://forum.xda-developers.com/apps/supersu",
      "pageTitle": "SuperSU",
      "checkUrl": "",
      "find": "/\\[stable\\]\\[(\\d{2,4})\\.(\\d{1,2})\\.(\\d{1,2})\\]\\s*supersu\\s*v?(\\d{1,2}.+?)</i",
      "replace": "SuperSU $4 от $3.$2.$1",
      "invalid": false
    },
    {
      "pageUrl": "http://forum.xda-developers.com/showthread.php?t=2778248",
      "pageTitle": "Khaon's Kernel F2FS For Nexus 10",
      "checkUrl": "[OLD]",
      "find": "/source\\scode.{1,20}href=\"https:\\/\\/github\\.com\\/khaon\\/kernel_samsung_manta\"[\\s\\S]{1,500}version\\sinformation[\\s\\S]{1,100}stable\\srelease\\sdate:.+?(\\d{2,4}).(\\d{1,2}).(\\d{1,2})[\\s\\S]{1,200}<\\/div>/i",
      "replace": "Khaon's Kernel от $3.$2.$1",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://wiki.cyanogenmod.org/w/Manta_Info",
      "pageTitle": "CyanogenMod for Nexus 10",
      "checkUrl": "[OLD]",
      "find": "/<td>[\\s\\S]{1,20}download.*?\\n.*?<a.*?>(cm-\\d{2}.*?manta)\\.\\w{2,4}<\\/a>/i",
      "replace": "$1",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://forum.xda-developers.com/android/software/reborn-gapps-5-t3074660",
      "pageTitle": "[OLD] PA-Google Apps 5.x Plus",
      "checkUrl": "[OLD]",
      "find": "/\\[gapps\\]\\[(\\d{1,2}).(\\d{1,2}).(\\d{2,4})\\]\\[5\\.\\w\\]\\spa-google\\sapps\\splus/i",
      "replace": "PA-Google Apps Plus от $1.$2.$3",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://1c.ru/rus/support/release/categ.jsp?GroupID=87&82",
      "pageTitle": "1С: Технологическая платформа 8.2",
      "checkUrl": "[OLD]",
      "find": "/<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(технологическая платформа)<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(8\\.2\\.\\d{1,2}\\.\\d{1,4})<\\//i",
      "replace": "$2 - версия $3 от $1",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      }
    },
    {
      "pageUrl": "http://forum.xda-developers.com/paranoid-android/general/gapps-official-to-date-pa-google-apps-t2943900",
      "pageTitle": "[OLD] Official PA Google Apps 5.x",
      "checkUrl": "[OLD]",
      "find": "/\\[gapps\\]\\[5\\.\\d.+(official\\sup-to-date.+)\\s\\[(\\d{2,4}).(\\d{1,2}).(\\d{1,2})\\]/i",
      "invalid": {
        "checkUrl": "Invalid URL syntax"
      },
      "replace": "$1 от $4.$3.$2"
    },
    {
      "pageUrl": "http://1c.ua/support/release.php?unf",
      "pageTitle": "1С: Конфигурация «Управление небольшой фирмой для Украины»",
      "checkUrl": "http://downloads.1c.ru/release_info/categ_js_ua.jsp?GroupID=97&amp;nohead=1",
      "find": "/<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2})<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(управление небольшой фирмой для украины)<\\/.+?;[\\n\\r]+.+?<td.*?><span.*?>(\\d{1,2}\\.\\d{1,2}\\.\\d{1,3}\\.\\d{1,3})<\\//i",
      "replace": "Конфигурация «$2» - версия $3 от $1",
      "invalid": false
    },
    {
      "pageUrl": "http://opencv.org",
      "pageTitle": "OpenCV Library",
      "checkUrl": "http://opencv.org/releases.html",
      "find": "/<div\\s+class=\"release-body\"[\\s\\S]{100,200}<div class=\"release-name\".*?>\\s*.*?(\\d{1,2}\\.\\d{1,2}\\.\\d.*)\\s*<\\/div>[\\s\\S]{1,50}<div class=\"release-date\".*?>\\s*(\\d{2,4})-(\\d{1,2})-(\\d{1,2})\\s*<\\/div>/i",
      "replace": "OpenCV $1 от $4.$3.$2",
      "invalid": false
    },
    {
      "pageUrl": "https://hub.docker.com/r/_/alpine",
      "pageTitle": "Alpine Linux Docker image",
      "checkUrl": "",
      "find": "/<code>(\\d+\\.\\d+.*)<\\/code>.{0,10}<code>latest<\\/code>/",
      "replace": "Latest: alpine:$1",
      "invalid": false
    }
  ]
};

console.debug('safari.extension.settings.settingsStorage initialized.');
