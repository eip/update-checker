/*global safari: false*/
(function () {
  'use strict';

  function isDefined(path, nonEmpty) {
    var object, index, key;

    for (index = 0, object = window, path = path.split('.'); index < path.length; index++) {
      object = object[path[index]];
      if (!object) {
        return false;
      }
    }
    if (nonEmpty) {
      for (key in object) {
        if (object.hasOwnProperty(key)) {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  function handleBlur(event) {
    if (isDefined('safari.extension.globalPage.contentWindow.lib.cancelResetNewUpdates')) {
      safari.extension.globalPage.contentWindow.lib.cancelResetNewUpdates();
    }
  }

  function openUrl(url) {
    var urlTab;

    if (url && url.target && url.target.href) {
      url = url.target.href;
    }
    if (typeof url !== 'string') {
      return;
    }
    safari.application.browserWindows.forEach(function (w) {
      w.tabs.forEach(function (t) {
        if (t.url === url) {
          urlTab = t;
        }
      });
    });
    if (!urlTab) {
      urlTab = safari.application.activeBrowserWindow.openTab();
      urlTab.url = url;
    }
    urlTab.browserWindow.activate();
    urlTab.activate();
    safari.self.hide();
    return false;
  }

  function initPublicFunctions() {
    window.lib = window.lib || {};

    window.lib.buildHistoryPage = function () {
      var divContainer, divItem, divSummary, divGutter, divContent, divDetails, spanNoHistory, toggleId,
        checkHistory, sortedUrls, url, urlIndex, historyRecs, historyRecCnt, historyRecIdx;

      function formatTimestamp(timestamp) {
        var options;

        timestamp = new Date(timestamp);
        options = {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'};
        return timestamp.toLocaleString('uk-UA', options);
      }

      function makeElementId(id, index) {
        return id + '-' + ('0' + (index + 1)).slice(-2);
      }

      function clearElement(element) {
        while (element && element.lastChild) {
          element.removeChild(element.lastChild);
        }
      }

      function createElement(name, id, className, attributes) {
        var element, attribute;

        element = document.createElement(name);
        if (id) {
          element.id = id;
        }
        if (className) {
          element.className = className;
        }
        for (attribute in attributes) {
          if (attributes.hasOwnProperty(attribute)) {
            element[attribute] = attributes[attribute];
          }
        }
        return element;
      }

      function createHistoryRecord(historyRecord) {
        var element;

        element = createElement('p');
        element.appendChild(createElement('span', '', 'date' + (historyRecord.isNew ? ' new' : '') + (historyRecord.isError ? ' error' : ''), {
          innerText: formatTimestamp(historyRecord.timestamp)
        }));
        element.appendChild(document.createTextNode(historyRecord.text));
        return element;
      }

      function getPageSettings(url) {
        var settings, pageIndex;

        if (isDefined('safari.extension.settings.settingsStorage.pages.length')) {
          settings = safari.extension.settings.settingsStorage.pages;
          for (pageIndex = 0; pageIndex < settings.length; ++pageIndex) {
            if (settings[pageIndex].pageUrl === url) {
              return settings[pageIndex];
            }
          }
        }
        return {};
      }

      divContainer = document.getElementById('container');
      if (!divContainer) {
        divContainer = createElement('div', 'container', 'container');
        document.body.insertBefore(divContainer, document.body.firstChild);
      }
      clearElement(divContainer);
      if (!isDefined('safari.extension.settings.checkHistory', true)) {
        spanNoHistory = createElement('span', '', 'nohistory');
        spanNoHistory.appendChild(document.createTextNode('No records in history. Check the '));
        spanNoHistory.appendChild(createElement('a', '', '', {
          href: safari.extension.baseURI + 'settings.html',
          onclick: openUrl
        })).appendChild(document.createTextNode('extension\'s settings'));
        spanNoHistory.appendChild(document.createTextNode('.'));
        divContainer.appendChild(spanNoHistory);
        return;
      }
      checkHistory = safari.extension.settings.checkHistory;
      sortedUrls = [];
      for (url in checkHistory) {
        if (checkHistory.hasOwnProperty(url)) {
          sortedUrls.push({
            url: url,
            timestamp: checkHistory[url][checkHistory[url].length - 1].timestamp
          });
        }
      }
      sortedUrls = sortedUrls.sort(function (a, b) {
        return b.timestamp - a.timestamp;
      });

      for (urlIndex = 0; urlIndex < sortedUrls.length; ++urlIndex) {
        url = sortedUrls[urlIndex].url;
        historyRecs = checkHistory[url].slice(-99);
        historyRecCnt = historyRecs.length;

        divItem = createElement('div', makeElementId('item', urlIndex), 'item');
        divSummary = createElement('div', '', 'summary');
        divGutter = createElement('div', '', 'gutter');

        if (historyRecCnt > 1) {
          toggleId = makeElementId('toggle', urlIndex);
          divItem.appendChild(createElement('input', toggleId, 'toggle', {
            name: toggleId,
            type: 'checkbox'
          }));
          divGutter.appendChild(createElement('label', '', '', {
            htmlFor: toggleId,
            innerText: historyRecCnt + '\u00a0'
          }));
        }
        divSummary.appendChild(divGutter);
        divContent = createElement('div', '', 'content');
        divContent.appendChild(createElement('a', '', '', {
          href: url,
          innerText: getPageSettings(url).pageTitle || url,
          onclick: openUrl
        }));
        divContent.appendChild(createHistoryRecord(historyRecs[historyRecCnt - 1]));
        divSummary.appendChild(divContent);
        divItem.appendChild(divSummary);

        if (historyRecCnt > 1) {
          divDetails = createElement('div', '', 'details');
          for (historyRecIdx = historyRecCnt - 2; historyRecIdx >= 0; --historyRecIdx) {
            divDetails.appendChild(createHistoryRecord(historyRecs[historyRecIdx]));
          }
          divItem.appendChild(divDetails);
        }
        divContainer.appendChild(divItem);
      }
      if (isDefined('safari.extension.globalPage.contentWindow.lib.queryResetNewUpdates')) {
        safari.extension.globalPage.contentWindow.lib.queryResetNewUpdates();
      }
    };
  }

  function initEventListeners() {
    window.addEventListener('blur', handleBlur, false);
  }

  initPublicFunctions();
  initEventListeners();
}());