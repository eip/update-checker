/*global safari: false*/
(function () {
  'use strict';
  var runCheckTimeout, clearUpdatesTimeout, animateTBIconTimeout, tbIconState, skipOpenSettingsEvent, updatesCount,
    maxHistoryTextLength, largeDelay, smallDelay, aMinute, strings, logInfo, logWarn, logError;

  function setConstants() {
    maxHistoryTextLength = 200;
    largeDelay = 3000;
    smallDelay = 200;
    aMinute = 60000;
    strings = {
      log: {
        checkIntervalIsNotDefined: 'Check interval is not defined',
        checkingCycleFinished: 'Checking cycle finished',
        checkingForPageUpdates: 'Checking for page updates',
        handledMessage: 'Handled message',
        justBeforeCheckingUpdates: 'Just {0} before checking updates',
        newUpdatesInfoWasReset: 'New updates info was reset',
        noPageSettingsFound: 'No page settings found',
        openingSettingsPage: 'Opening settings page',
        pageSkippedFromChecking: 'Page skipped from checking',
        queryForResetNewUpdatesInfoWasCanceled: 'Query for reset new updates info was canceled',
        requestAborted: 'Request aborted by timeout',
        requestingPageText: 'Requesting page text',
        resetNewUpdatesInfoQueried: 'Reset new updates info queried',
        unhandledMessage: 'Unhandled message'
      },
      failedToLoadPageContent: 'Failed to load page content',
      hour: 'hour',
      hours: 'hours',
      incorrectReplacePattern: 'Incorrect replace pattern',
      minute: 'minute',
      minutes: 'minutes',
      networkError: 'Network Error',
      noMatchInPageText: 'No match in page text',
      noMatchesWithRegularExpression: 'No matches with regular expression',
      pageUpdated: 'Page updated',
      toolTipDefault: 'Update checker',
      toolTipChecking: 'Update checker: checking',
      toolTipJustFinished: 'Update checker: just finished',
      toolTip: 'Update checker: next check in {0}'
    };
  }

  function isDefined(path, nonEmpty) {
    var object, index, key;

    for (index = 0, object = window, path = path.split('.'); index < path.length; index++) {
      object = object[path[index]];
      if (!object) {
        return false;
      }
    }
    if (nonEmpty) {
      for (key in object) {
        if (object.hasOwnProperty(key)) {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  function setLogging() {

    function noop() {}

    var minLevel;

    if (!isDefined('safari.extension.settings.settingsStorage')) {
      minLevel = 1;
    } else {
      minLevel = parseInt(safari.extension.settings.settingsStorage.logLevel, 10);
      minLevel = isNaN(minLevel) ? 1 : Math.min(Math.max(minLevel, 0), 2);
    }
    logInfo = minLevel ? noop : window.console.info.bind(window.console);
    logWarn = minLevel > 1 ? noop : window.console.warn.bind(window.console);
    logError = window.console.error.bind(window.console);
  }

  function addTS(text) {
    var nowDate;

    nowDate = new Date();
    return [nowDate.toDateString().split(' ').slice(1, 3).join(' '), nowDate.toTimeString().split(' ')[0], '>', text].join(' ');
  }

  function formatString(format) {
    var args;

    args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/\{(\d+)\}/g, function (match, number) {
      return typeof args[number] !== 'undefined' ? args[number] : match;
    });
  }

  function getTimeIntervalInMinutes(value) {
    var result, hours, minutes;

    value = Math.round(value);
    hours = Math.floor(value / 60);
    minutes = value % 60;
    result = [];
    if (hours) {
      result.push(hours + ' ' + (hours > 1 ? strings.hours : strings.hour));
    }
    if (minutes) {
      result.push(minutes + ' ' + (minutes > 1 ? strings.minutes : strings.minute));
    }
    return result.join(' ');
  }

  function initialSetup() {
    var settingsStorage;

    if (!isDefined('safari.extension.settings.settingsStorage.pages')) {
      settingsStorage = safari.extension.settings.settingsStorage || {};
      settingsStorage.checkInterval = settingsStorage.checkInterval || 60;
      settingsStorage.pages = [{
        'pageUrl': 'https://www.apple.com/support/osx/software-updates',
        'pageTitle': 'Example: OS X — Recent Software Updates',
        'find': '/<li class=\"portlet-results dt-downloads\">[\\s\\S]{1,200}<h3>.*?<span>(.+?)<\\/span>.*?<\\/h3>[\\s\\S]{1,100}<div.*?>.*?<span.*?>([\\d\\/]+?)<\\/span>/i',
        'replace': '$1 from $2',
        'checkUrl': '',
        'invalid': false
      }];
      safari.extension.settings.settingsStorage = settingsStorage;
    }
  }

  function startAnimateTBIcon(stop) {
    var tbIconUrl;

    clearTimeout(animateTBIconTimeout);
    tbIconState = stop ? 0 : (tbIconState || 0) % 5;
    if (isDefined('safari.extension.toolbarItems.length')) {
      tbIconUrl = safari.extension.baseURI + 'toolbar-button' + (tbIconState ? '-0' + tbIconState : '') + '.png';
      safari.extension.toolbarItems.forEach(function (i) {
        if (i.image !== tbIconUrl) {
          i.badge = 0;
          i.image = tbIconUrl;
          i.badge = updatesCount;
        }
      });
      if (!stop) {
        tbIconState++;
        animateTBIconTimeout = setTimeout(startAnimateTBIcon, smallDelay);
      }
    }
  }

  function stopAnimateTBIcon() {
    startAnimateTBIcon(-1);
  }

  function showNotification(historyRecord, pageTitle) {
    var notification;

    if (window.Notification) {
      notification = new window.Notification(strings.pageUpdated + ': ' + pageTitle, {
        'body': historyRecord.text,
        'tag': historyRecord.timestamp
      });
      notification = null;
    }
  }

  function updateBadge() {
    if (isDefined('safari.extension.toolbarItems.length')) {
      safari.extension.toolbarItems.forEach(function (i) {
        i.badge = updatesCount;
      });
    }
  }

  function setTooltip(text) {
    if (isDefined('safari.extension.toolbarItems.length')) {
      safari.extension.toolbarItems.forEach(function (i) {
        i.toolTip = text || strings.toolTipDefault;
      });
    }
  }

  function calcUpdatesCount() {
    var checkHistory, modified, url, i;

    updatesCount = 0;
    checkHistory = safari.extension.settings.checkHistory;
    for (url in checkHistory) {
      if (checkHistory.hasOwnProperty(url)) {
        for (i = checkHistory[url].length - 1; i >= 0; --i) {
          if (checkHistory[url][i].isNew) {
            ++updatesCount;
          } else {
            break;
          }
        }
      }
    }
    if (updatesCount) {
      updateBadge();
    }
  }

  function initPublicFunctions() {
    window.lib = window.lib || {};

    window.lib.openSettingsPage = function () {
      var settingsTab, settingsUrl;

      settingsUrl = safari.extension.baseURI + 'settings.html';
      safari.application.browserWindows.forEach(function (w) {
        w.tabs.forEach(function (t) {
          if (t.url === settingsUrl) {
            settingsTab = t;
          }
        });
      });
      if (!settingsTab) {
        settingsTab = safari.application.activeBrowserWindow.openTab();
        settingsTab.url = settingsUrl;
      }
      settingsTab.browserWindow.activate();
      settingsTab.activate();
    };

    window.lib.queryResetNewUpdates = function (cancel) {

      function removeIsNewMarks() {
        var checkHistory, modified, url, i;

        if (!isDefined('safari.extension.settings.checkHistory', true)) {
          return;
        }
        modified = false;
        checkHistory = safari.extension.settings.checkHistory;
        for (url in checkHistory) {
          if (checkHistory.hasOwnProperty(url)) {
            for (i = checkHistory[url].length - 1; i >= 0; --i) {
              if (checkHistory[url][i].isNew) {
                checkHistory[url][i].isNew = false;
                modified = true;
              } else {
                break;
              }
            }
          }
        }
        if (modified) {
          safari.extension.settings.checkHistory = checkHistory;
        }
      }

      clearTimeout(clearUpdatesTimeout);
      if (!updatesCount) {
        return;
      }
      if (!cancel) {
        logInfo(addTS(strings.log.resetNewUpdatesInfoQueried));
        clearUpdatesTimeout = setTimeout(function () {
          removeIsNewMarks();
          updatesCount = 0;
          updateBadge();
          logInfo(addTS(strings.log.newUpdatesInfoWasReset));
        }, largeDelay);
      } else {
        logInfo(addTS(strings.log.queryForResetNewUpdatesInfoWasCanceled));
      }
    };

    window.lib.cancelResetNewUpdates = function () {
      window.lib.queryResetNewUpdates(-1);
    };
  }

  function run() {
    var nowTime;

    function nextLoop() {
      stopAnimateTBIcon();
      runCheckTimeout = setTimeout(run, aMinute);
    }

    function noop(data) {
      return window.Promise.resolve(data);
    }

    function checkConnection() {
      var urls, urlsLeft, connected;

      function checkUrl(url) {
        return new window.Promise(function (resolve, reject) {
          var request, aborted, abortTimeout;

          request = new XMLHttpRequest();
          aborted = false;
          request.onreadystatechange = function () {
            if (connected) {
              if (request) {
                clearTimeout(abortTimeout);
                request.abort();
                request = null;
              }
            } else {
              if (request.readyState === 4) {
                clearTimeout(abortTimeout);
                if (!aborted && request.status === 200) {
                  connected = true;
                  resolve(url);
                } else {
                  --urlsLeft;
                  if (urlsLeft <= 0) {
                    reject(new Error(strings.networkError));
                  }
                }
                request = null;
              }
            }
          };
          if (typeof request.timeout === 'undefined') {
            abortTimeout = setTimeout(function () { // Safari 5.1 workaround
              aborted = true;
              request.abort();
              logWarn(addTS(strings.log.requestAborted));
            }, 10000);
          } else {
            request.timeout = 10000;
          }
          request.open('GET', url + '/?' + Date.now());
          request.send();
        });
      }

      if (window.navigator.onLine) {
        urls = ['http://www.apple.com', 'http://www.google.com'];
        urlsLeft = urls.length;
        connected = false;
        return window.Promise.race(urls.map(checkUrl));
      } else {
        return window.Promise.reject(new Error(strings.networkError));
      }
    }

    function ifTimeForCheck() {
      return new window.Promise(function (resolve, reject) {
        var timeLeft;

        if (!isDefined('safari.extension.settings.settingsStorage.pages.length')) {
          logWarn(addTS(strings.log.noPageSettingsFound));
          reject();
        }
        if (!isDefined('safari.extension.settings.settingsStorage.checkInterval')) {
          logWarn(addTS(strings.log.checkIntervalIsNotDefined));
          reject();
        }
        nowTime = Math.round(Date.now() / aMinute);
        if (nowTime >= (safari.extension.settings.lastCheck || 0) + safari.extension.settings.settingsStorage.checkInterval) {
          setTooltip();
          resolve();
        } else {
          timeLeft = safari.extension.settings.lastCheck + safari.extension.settings.settingsStorage.checkInterval - nowTime;
          setTooltip(formatString(strings.toolTip, getTimeIntervalInMinutes(timeLeft)));
          if ((timeLeft % 10 === 0) || ((timeLeft < 30) && (timeLeft % 5 === 0)) || (timeLeft < 5)) {
            logInfo(addTS(formatString(strings.log.justBeforeCheckingUpdates, getTimeIntervalInMinutes(timeLeft))));
          }
          reject();
        }
      });
    }

    function checkPages() {
      var checkQueue;

      function getCheckQueue() {
        // Fill empty checkURLs, skip invalid pages
        return safari.extension.settings.settingsStorage.pages.reduce(function (queue, pageSettings) {
          pageSettings.checkUrl = pageSettings.checkUrl || pageSettings.pageUrl;
          if (pageSettings.checkUrl && pageSettings.invalid === false && pageSettings.find) {
            queue.push(pageSettings);
          } else {
            logWarn(addTS(strings.log.pageSkippedFromChecking + ': ' + pageSettings.pageUrl));
          }
          return queue;
        }, []);
      }

      function checkPage(pageSettings) {

        function sendRequest(pageSettings) {
          return new window.Promise(function (resolve, reject) {
            var request, aborted, abortTimeout;

            // logInfo(addTS(strings.log.requestingPageText + ': ' + pageSettings.checkUrl));
            request = new XMLHttpRequest();
            aborted = false;
            request.onreadystatechange = function () {
              if (request.readyState === 4) {
                clearTimeout(abortTimeout);
                if (!aborted && request.status === 200 && request.responseText) {
                  pageSettings.text = request.responseText;
                  request = null;
                  resolve(pageSettings);
                } else {
                  request = null;
                  pageSettings.error = strings.failedToLoadPageContent + ': ' + pageSettings.checkUrl;
                  reject(new Error(pageSettings.error));
                }
              }
            };
            if (typeof request.timeout === 'undefined') {
              abortTimeout = setTimeout(function () { // Safari 5.1 workaround
                aborted = true;
                request.abort();
                logWarn(addTS(strings.log.requestAborted));
              }, 30000);
            } else {
              request.timeout = 30000;
            }
            request.open('GET', pageSettings.checkUrl);
            request.send();
          });
        }

        function extractText(pageSettings) {
          return new window.Promise(function (resolve, reject) {
            var re, match, i;

            try {
              match = /^\/(.+)\/(\w{0,3})$/.exec(pageSettings.find);
              if (!match) {
                re = new RegExp(pageSettings.find);
              } else {
                re = new RegExp(match[1], match[2]);
              }
              match = re.exec(pageSettings.text);
              pageSettings.text = '';
              if (match && match.length > 0) {
                pageSettings.text = match[0];
                if (!pageSettings.replace && match.length > 1) {
                  pageSettings.replace = Array.apply(null, new Array(match.length - 1)).map(function (e, i) {
                    return '$' + (++i);
                  }).join(' ');
                }
                if (pageSettings.replace) {
                  pageSettings.text = pageSettings.text.replace(re, pageSettings.replace);
                  if (pageSettings.text === pageSettings.replace) {
                    pageSettings.text = '';
                    pageSettings.error = strings.incorrectReplacePattern + ': ' + pageSettings.replace;
                    reject(new Error(pageSettings.error));
                    return;
                  }
                }
                if (pageSettings.text.length > maxHistoryTextLength) {
                  pageSettings.text = (pageSettings.text.substring(0, maxHistoryTextLength - 1) + '…');
                }
              } else {
                pageSettings.error = strings.noMatchesWithRegularExpression + ': ' + pageSettings.find;
                reject(new Error(pageSettings.error));
                return;
              }
              resolve(pageSettings);
            } catch (err) {
              pageSettings.text = '';
              pageSettings.error = err.message;
              reject(err);
            }
          });
        }

        function recordToHistory(pageSettings) {
          var newRecord, allHistory, pageHistory, historyChanged;

          function getLastHistoryRecord() {
            if (!pageHistory) {
              pageHistory = [];
              historyChanged = true;
              return {};
            }
            while (pageHistory.length > 0) {
              if (!pageHistory[pageHistory.length - 1].isError) {
                return pageHistory[pageHistory.length - 1];
              }
              pageHistory.pop();
              historyChanged = true;
            }
            return {};
          }

          allHistory = safari.extension.settings.checkHistory || {};
          pageHistory = allHistory[pageSettings.pageUrl];
          historyChanged = false;
          newRecord = {
            timestamp: nowTime * aMinute,
            text: pageSettings.error || pageSettings.text
          };
          if (getLastHistoryRecord().text !== pageSettings.text || pageSettings.error) {
            if (pageSettings.error) {
              newRecord.isError = true;
            } else {
              newRecord.isNew = true;
            }
            pageHistory.push(newRecord);
            historyChanged = true;
          }
          if (historyChanged) {
            allHistory[pageSettings.pageUrl] = pageHistory.slice(-200);
            safari.extension.settings.checkHistory = allHistory;
          }
          return window.Promise.resolve(newRecord);
        }

        function notifyOfUpdates(historyRecord) {
          if (historyRecord.isNew) {
            updatesCount++;
            updateBadge();
            showNotification(historyRecord, pageSettings.pageTitle);
            logInfo(addTS(strings.pageUpdated + ': ' + pageSettings.pageUrl + ' - ' + historyRecord.text));
          }
          return window.Promise.resolve();
        }

        logInfo(addTS(strings.log.checkingForPageUpdates + ': ' + pageSettings.pageUrl));
        return sendRequest(pageSettings).then(extractText).then(noop, function (err) {
          if (err instanceof Error) {
            logError(addTS(err.message));
            return window.Promise.resolve(pageSettings);
          }
        }).then(recordToHistory).then(notifyOfUpdates);
      }

      checkQueue = getCheckQueue();
      safari.extension.settings.lastCheck = nowTime;
      setTooltip(strings.toolTipChecking);
      startAnimateTBIcon();
      return window.Promise.all(checkQueue.map(function (pageSettings) {
        return checkPage(pageSettings);
      }));
    }

    setLogging();
    stopAnimateTBIcon();
    clearTimeout(runCheckTimeout);
    ifTimeForCheck().then(checkConnection).then(checkPages).then(function (data) {
      setTooltip(strings.toolTipJustFinished);
      return noop(data);
    }, function (err) {
      if (err instanceof Error) {
        logError(addTS(err.message));
      }
      return window.Promise.resolve();
    }).then(nextLoop);
  }

  function handleMessage(event) {
    event.stopPropagation();
    switch (event.name) {
    case 'getSettingsStorage':
      event.target.page.dispatchMessage('getSettingsStorage', safari.extension.settings.settingsStorage || {});
      logInfo(addTS(strings.log.handledMessage + ': ' + event.name));
      break;
    case 'setSettingsStorage':
      safari.extension.settings.settingsStorage = event.message;
      logInfo(addTS(strings.log.handledMessage + ': ' + event.name));
      break;
    case 'getCheckHistory':
      event.target.page.dispatchMessage('getCheckHistory', safari.extension.settings.checkHistory || {});
      logInfo(addTS(strings.log.handledMessage + ': ' + event.name));
      break;
    case 'setCheckHistory':
      safari.extension.settings.checkHistory = event.message;
      logInfo(addTS(strings.log.handledMessage + ': ' + event.name));
      break;
    default:
      logWarn(addTS(strings.log.unhandledMessage + ': ' + event.name));
    }
  }

  function openPopover(event) {
    event.stopPropagation();
    if (event.target.identifier === 'popover') {
      event.stopPropagation();
      event.target.contentWindow.lib.buildHistoryPage();
    }
  }

  function settingsChange(event) {
    event.stopPropagation();
    if (event.key === 'openSettings') {
      event.stopPropagation();
      if (!skipOpenSettingsEvent) {
        skipOpenSettingsEvent = true;
        logInfo(addTS(strings.log.openingSettingsPage));
        event.target[event.key] = false;
        window.lib.openSettingsPage();
        setTimeout(function () {
          skipOpenSettingsEvent = false;
        }, largeDelay);
      }
    }
  }

  function initEventListeners() {
    safari.application.addEventListener('message', handleMessage, true);
    safari.application.addEventListener('popover', openPopover, true);
    safari.extension.settings.addEventListener('change', settingsChange, true);
  }

  setConstants();
  setLogging();
  initialSetup();
  initPublicFunctions();
  initEventListeners();
  calcUpdatesCount();
  run();
}());