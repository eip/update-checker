/* global safari: false */
/* eslint no-bitwise: "off" */
(function main() {
  'use strict';

  var maxHistoryTextLength, maxLogTextLength, smallDelay, strings, pageOptions, logInfo, logWarn, logError;

  function setConstants() {
    maxHistoryTextLength = 200;
    maxLogTextLength = 30000;
    smallDelay = 200;
    strings = {
      labels: {
        checkInterval: 'Check Interval',
        pageUrl: 'Page URL',
        pageTitle: 'Page Title',
        checkUrl: 'Check URL',
        find: 'Find What',
        replace: 'Replace With',
        newPageUrl: 'New Page URL'
      },
      placeholders: {
        pageTitle: 'Title to display in history',
        checkUrl: 'URL to check if it\'s different from the page URL',
        find: 'Regular expression to extract essential information from the page text',
        replace: 'Replace pattern (e.g., "$&" or "$2: version $3 from $1")',
        newPageUrl: 'New page URL (it must be unique)'
      },
      tooltips: {
        delete: 'Remove this page from checking and delete all settings for it',
        error: 'Page is unavailable or its settings are invalid.\nYou may fix page settings and test it again',
        ok: 'Page settings validated successfully.\nYou may test page settings again',
        test: 'Test page availability and validate its settings'
      },
      log: {
        handledMessage: 'Handled message',
        importSettingsError: 'Import Settings Error',
        noFunctionToValidate: 'No function to validate',
        pageInitializationCompleted: 'Page initialization completed',
        requestAborted: 'Request aborted by timeout',
        serviceUnavailable: 'Service unavailable',
        settingsInvalidElementID: 'Settings: Invalid element ID',
        settingsInvalidPageIndex: 'Settings: Invalid page index',
        settingsNoPageSettingsFound: 'Settings: No page settings found',
        settingsStorageNotDefined: 'Settings storage object is not defined',
        unhandledMessage: 'Unhandled message'
      },
      applyingReplacePattern: 'Applying replace pattern',
      cancelLbl: 'Cancel',
      errorCreatingImportDialog: 'Error creating import dialog',
      errorCreatingTestDialog: 'Error creating test dialog',
      exportSettings: 'Export Settings',
      extractedText: 'Extracted text',
      failedToLoadPageContent: 'Failed to load page content',
      firstCharactersAreShown: 'First {0} characters from {1} are shown',
      gotResult: 'got result',
      hour: 'hour',
      hours: 'hours',
      importLbl: 'Import',
      importSettings: 'Import Settings',
      incorrectReplacePattern: 'Incorrect replace pattern',
      invalidUrlSyntax: 'Invalid URL syntax',
      minute: 'minute',
      minutes: 'minutes',
      networkError: 'Network Error',
      noMatchesWithRegEx: 'No matches with regular expression',
      noMatchesWithThisRegEx: 'No matches with this regular expression',
      pageContentUnavailable: 'Page content unavailable',
      pasteExportedSettingsTextHere: 'Paste exported settings text here...',
      regexCantBeEmpty: 'Regular expression can\'t be empty',
      requestingPageContent: 'Requesting page content',
      searchingPageTextFor: 'Searching page text for',
      status: 'Status',
      testDialogClosed: 'Test dialog was closed',
      testFailed: 'Test failed!',
      testPassed: 'Test passed successfully!',
      textFoundAtPosition: 'Text found at position',
      truncatingResultText: 'Truncating result text',
      urlAlreadyExists: 'Entered URL already exists in page list',
      urlCantBeEmpty: 'URL can\'t be empty'
    };
    pageOptions = [{
      id: 'pageUrl',
      name: strings.labels.pageUrl,
      className: 'url',
      ext: {
        type: 'text',
        readOnly: true
      }
    }, {
      id: 'pageTitle',
      name: strings.labels.pageTitle,
      className: 'text',
      ext: {
        type: 'text',
        placeholder: strings.placeholders.pageTitle
      }
    }, {
      id: 'checkUrl',
      name: strings.labels.checkUrl,
      label: strings.labels.checkUrl + '<sup>*</sup>',
      className: 'text',
      ext: {
        type: 'text',
        placeholder: strings.placeholders.checkUrl
      }
    }, {
      id: 'find',
      name: strings.labels.find,
      className: 'text',
      ext: {
        type: 'text',
        placeholder: strings.placeholders.find
      }
    }, {
      id: 'replace',
      name: strings.labels.replace,
      className: 'text',
      ext: {
        type: 'text',
        placeholder: strings.placeholders.replace
      }
    }];
  }

  function isDefined(path, nonEmpty) {
    var object, index;

    for (index = 0, object = window, path = path.split('.'); index < path.length; index++) {
      object = object[path[index]];
      if (!object) {
        return false;
      }
    }
    if (nonEmpty) {
      return !!Object.keys(object).length;
    }
    return true;
  }

  function setLogging() {
    var minLevel;

    if (!isDefined('settingsStorage')) {
      minLevel = 1;
    } else {
      minLevel = parseInt(window.settingsStorage.logLevel, 10);
      minLevel = Number.isNaN(minLevel) ? 1 : Math.min(Math.max(minLevel, 0), 2);
    }
    logInfo = minLevel ? function _logInfo() {} : window.console.info.bind(window.console);
    logWarn = minLevel > 1 ? function _logWarn() {} : window.console.warn.bind(window.console);
    logError = window.console.error.bind(window.console);
  }

  function addTS(text) {
    var nowDate;

    nowDate = new Date();
    return [nowDate.toDateString().split(' ').slice(1, 3).join(' '), nowDate.toTimeString().split(' ')[0], '>', text].join(' ');
  }

  function addFileNameTS(name, ext) {
    var nowDate;

    nowDate = new Date();
    return [
      [name, [nowDate.getFullYear(), ('0' + (nowDate.getMonth() + 1)).slice(-2), ('0' + nowDate.getDate()).slice(-2)].join(''), [('0' + nowDate.getHours()).slice(-2), ('0' + nowDate.getMinutes()).slice(-2), ('0' + nowDate.getSeconds()).slice(-2)].join('')].join('-'), ext
    ].join('.');
  }

  function getInt(value, defaultValue) {
    var result;

    result = parseInt(value, 10);
    if (Number.isNaN(result)) {
      return getInt(defaultValue, 0);
    }
    return result;
  }

  function getString(value, defaultValue) {
    if (value !== 0 && !value) {
      return String(defaultValue || '');
    }
    return String(value);
  }

  function formatString(format) {
    var args;

    args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/\{(\d+)\}/g, function _replace(match, number) {
      return typeof args[number] !== 'undefined' ? args[number] : match;
    });
  }

  function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%[0-9A-F]{2}/g, function _toSolidBytes(match) {
      return String.fromCharCode('0x' + match.slice(1));
    }));
  }

  function getTimeIntervalInMinutes(value) {
    var transcript, hours, minutes;

    value = Math.round(value);
    hours = Math.floor(value / 60);
    minutes = value % 60;
    transcript = '';
    if (hours) {
      transcript = [];
      transcript.push(hours + ' ' + (hours > 1 ? strings.hours : strings.hour));
      if (minutes) {
        transcript.push(minutes + ' ' + (minutes > 1 ? strings.minutes : strings.minute));
      }
      transcript = ' (' + transcript.join(' ') + ')';
    }
    return [value > 1 ? strings.minutes : strings.minute] + transcript;
  }

  function isValidUrlSyntax(url) {
    // Regex from https://gist.github.com/dperini/729294
    var re;

    re = new RegExp('^' +
      // protocol identifier
      '(?:https?://)' +
      // user:pass authentication
      '(?:\\S+(?::\\S*)?@)?' +
      '(?:' +
      // IP address exclusion
      // private & local networks
      '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
      '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
      '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
      // IP address dotted notation octets
      // excludes loopback network 0.0.0.0
      // excludes reserved space >= 224.0.0.0
      // excludes network & broacast addresses
      // (first & last IP address of each class)
      '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
      '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
      '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
      '|' +
      // host name
      '(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)' +
      // domain name
      '(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*' +
      // TLD identifier
      '(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))' +
      ')' +
      // port number
      '(?::\\d{2,5})?' +
      // resource path
      '(?:/\\S*)?' +
      '$', 'i');
    return re.test(url);
  }

  function validate(option, value) {
    var validator;

    validator = {
      checkInterval: function _checkInterval() {
        var newValue;

        newValue = getInt(value, 60);
        if (newValue > 60 * 24) {
          newValue = 60 * 24;
        } else if (newValue < 5) {
          newValue = 5;
        }
        return {
          value: newValue
        };
      },
      pageTitle: function _pageTitle() {
        var newValue;

        newValue = getString(value).trim();
        return {
          value: newValue
        };
      },
      checkUrl: function _checkUrl() {
        var result;

        result = {};
        result.value = getString(value).replace(/[\s/]+$/, '').trim();
        if (result.value && !isValidUrlSyntax(result.value)) {
          result.error = strings.invalidUrlSyntax;
        }
        return result;
      },
      pageUrl: function _pageUrl() {
        var result;

        result = validator.checkUrl();
        if (!result.value) {
          result.error = strings.urlCantBeEmpty;
        }
        return result;
      },
      find: function _find() {
        var newValue, match, regex;

        newValue = getString(value).trim();
        if (!newValue) {
          return {
            value: newValue,
            error: strings.regexCantBeEmpty
          };
        }
        match = /^\/(.+)\/(\w{0,3})$/.exec(newValue);
        if (!match) {
          value = '/' + newValue + '/';
          return _find();
        }
        try {
          regex = new RegExp(match[1], match[2]);
          return {
            value: regex.toString()
          };
        } catch (err) {
          return {
            value: newValue,
            error: err.message
          };
        }
      },
      newPageUrl: function _newPageUrl() {
        var result, pageIndex;

        result = validator.pageUrl();
        if (result.error) {
          return result;
        }
        if (isDefined('settingsStorage.pages.length')) {
          for (pageIndex = 0; pageIndex < window.settingsStorage.pages.length; ++pageIndex) {
            if (result.value === window.settingsStorage.pages[pageIndex].pageUrl) {
              result.error = strings.urlAlreadyExists;
            }
          }
        }
        return result;
      }
    };
    validator.replace = validator.pageTitle;

    try {
      return validator[option]();
    } catch (err) {
      logError(addTS(strings.log.noFunctionToValidate + ' "' + option + '"'));
      return {
        value: value
      };
    }
  }

  function validatePageSettingsOption(pageSettings, optionId, clearInvalidMark) {
    var result;

    result = validate(optionId, pageSettings[optionId]);
    if (result.value !== pageSettings[optionId]) {
      pageSettings[optionId] = result.value;
      result.changed = true;
    }
    if (result.error) {
      pageSettings.invalid = pageSettings.invalid || {};
      pageSettings.invalid[optionId] = result.error;
    } else if (pageSettings.invalid && clearInvalidMark) {
      delete pageSettings.invalid[optionId];
      if (!Object.keys(pageSettings.invalid).length) {
        delete pageSettings.invalid;
      }
    } else if (result.changed && pageSettings.invalid === false) {
      delete pageSettings.invalid;
    }
    return result;
  }

  function isElement(object, nodeName) {
    return !!object && object instanceof window.HTMLElement && (!nodeName || object.nodeName === nodeName.toUpperCase());
  }

  function makeElementId(id, index) {
    return id + '-' + ('0' + (index + 1)).slice(-2);
  }

  function parseElementId(elementId) {
    var result, idIndex, id, index;

    idIndex = elementId.split('-');
    id = idIndex[0];
    if (!id) {
      logWarn(addTS(strings.log.settingsInvalidElementID + ': ' + elementId));
      return undefined;
    }
    result = {
      id: id,
      settings: window.settingsStorage
    };
    if (idIndex.length > 1) {
      index = getInt(idIndex[1]) - 1;
      if (index < 0 || index > 49) {
        logWarn(addTS(strings.log.settingsInvalidPageIndex + ': ' + elementId));
        return undefined;
      }
      result.index = index;
      result.settings = window.settingsStorage.pages[index];
    }
    return result;
  }

  function cleanUpSettings(settings) {
    var result;

    result = {};
    Object.keys(settings).filter(function _filter(option) {
      return option !== 'pages';
    }).forEach(function _forEach(option) {
      result[option] = settings[option];
    });
    result.pages = settings.pages.filter(function _filter(item) {
      return !!item;
    });
    return result;
  }

  function clearElement(element) {
    while (element && element.lastChild) {
      element.removeChild(element.lastChild);
    }
  }

  function createElement(name, id, className, attributes) {
    var element;

    element = document.createElement(name);
    if (id) {
      element.id = id;
    }
    if (className) {
      element.className = className;
    }
    Object.keys(attributes || {}).forEach(function _forEach(attribute) {
      element[attribute] = attributes[attribute];
    });
    return element;
  }

  function createOptionItem(label, id, className, attributes) {
    var liOption, div, input;

    liOption = createElement('li');
    div = createElement('div', '', 'label');
    liOption.appendChild(div);
    div.appendChild(createElement('label', '', id === 'newPageUrl' ? 'gray' : '', {
      htmlFor: attributes.id,
      innerHTML: label
    }));
    div = createElement('div', '', 'option');
    liOption.appendChild(div);
    input = createElement('input', id, className, attributes);
    if (input.classList.contains('text')) {
      input.onmouseover = handleShowTooltip;
      input.onmouseout = handleHideTooltip;
    }
    input.onchange = handleChangeOption;
    div.appendChild(input);
    if (id === 'checkInterval') {
      div.appendChild(createElement('span', '', 'unit', {
        innerText: getTimeIntervalInMinutes(attributes.value)
      }));
    }

    return liOption;
  }

  function showDialog() {
    var overlay;

    overlay = document.getElementById('overlay');
    if (overlay) {
      overlay.classList.add('visible');
    }
    document.body.classList.add('noscroll');
  }

  function closeDialog() {
    var overlay;

    overlay = document.getElementById('overlay');
    if (overlay) {
      overlay.classList.remove('visible');
      clearElement(overlay);
    }
    document.body.classList.remove('noscroll');
  }

  function createDialogBlock() {
    var divContainer, divOverlay, divDialog, divButtons;

    divContainer = document.getElementById('container');
    if (!divContainer) {
      return undefined;
    }
    divOverlay = document.getElementById('overlay');
    if (!divOverlay) {
      divOverlay = divContainer.appendChild(createElement('div', 'overlay', 'overlay'));
    } else {
      clearElement(divOverlay);
    }
    divDialog = createElement('div', 'dialog', 'dialog');
    divButtons = createElement('div', '', 'dialogwindowbuttons');
    divButtons.appendChild(createElement('div', '', 'dialogwindowbutton close', {
      onclick: closeDialog
    }));
    divButtons.appendChild(createElement('div', '', 'dialogwindowbutton'));
    divButtons.appendChild(createElement('div', '', 'dialogwindowbutton'));
    divDialog.appendChild(divButtons);
    divDialog.appendChild(createElement('div', '', 'dialogheader')).appendChild(createElement('span', 'dialogTitle', 'dialogtitle'));
    divOverlay.appendChild(divDialog);
    return divDialog;
  }

  function updatePageSettingsStatus(pageIndex) {
    var page, button, statusClass;

    page = document.getElementById(makeElementId('page', pageIndex));
    if (!isElement(page)) {
      return;
    }
    button = page.getElementsByClassName('button')[1];
    if (!isElement(button)) {
      return;
    }
    if (!window.settingsStorage.pages || !window.settingsStorage.pages[pageIndex]) {
      return;
    }
    if (typeof window.settingsStorage.pages[pageIndex].invalid === 'undefined') {
      statusClass = 'test';
    } else if (!window.settingsStorage.pages[pageIndex].invalid) {
      statusClass = 'ok';
    } else {
      statusClass = 'error';
    }
    page.className = 'page ' + statusClass;
    button.className = 'button ' + statusClass;
  }

  function addPageSettingsBlock(pageIndex) {
    var pageSettings, pageSettingsChanged, divNewPage, divPage, divActions, ulOptions, attributes;

    function classNameValid(item) {
      return item.className + (pageSettings.invalid && pageSettings.invalid[item.id] ? ' error' : '');
    }

    divNewPage = document.getElementById('newPage');
    if (!divNewPage) {
      return undefined;
    }
    pageSettings = window.settingsStorage.pages[pageIndex];
    pageSettingsChanged = false;
    divPage = createElement('div', makeElementId('page', pageIndex), 'page');

    divActions = createElement('div', '', 'actions');
    divActions.appendChild(createElement('div', '', 'button delete', {
      onmouseover: handleShowTooltip,
      onmouseout: handleHideTooltip,
      onclick: handlePageAction
    }));
    divActions.appendChild(createElement('div', '', 'button', {
      onmouseover: handleShowTooltip,
      onmouseout: handleHideTooltip,
      onclick: handlePageAction
    }));
    divPage.appendChild(divActions);

    ulOptions = createElement('ul', '', 'options');
    pageOptions.forEach(function _forEach(item) {
      attributes = item.ext;
      attributes.validated = validatePageSettingsOption(pageSettings, item.id);
      pageSettingsChanged = pageSettingsChanged || attributes.validated.changed;
      attributes.value = attributes.validated.value;
      delete attributes.validated;
      ulOptions.appendChild(createOptionItem((item.label || item.name) + ':', makeElementId(item.id, pageIndex), classNameValid(item), attributes));
    });
    divPage.appendChild(ulOptions);
    divNewPage.parentElement.insertBefore(divPage, divNewPage);

    updatePageSettingsStatus(pageIndex);
    if (pageSettingsChanged) {
      window.lib.setSettingsStorage();
    }
    return divPage;
  }

  function runPageSettingsTest(pageData) {
    var pageSettingsChanged;

    function addLogText(text, className, mark, noTrim, noLineBreak) {
      var divTestLog;

      divTestLog = document.getElementById('dialogTestLog');
      if (!(divTestLog && divTestLog.parentElement.parentElement.classList.contains('visible'))) {
        throw new Error(strings.testDialogClosed);
      }
      text = (text || '');
      if (!noTrim) {
        text = text.trim();
      }
      if (mark) {
        text = '> ' + text;
      }
      if (text.length > maxLogTextLength) {
        text = text.substring(0, maxLogTextLength - 1) + '…\n[' + formatString(strings.firstCharactersAreShown, maxLogTextLength, text.length) + ']';
      }
      divTestLog.appendChild(createElement('span', '', className || 'log', {
        innerText: noLineBreak ? text : text + '\n'
      }));
      divTestLog.scrollTop = divTestLog.scrollHeight;
    }

    function validatePageSettings() {
      var result, validated, input;

      result = false;
      pageOptions.forEach(function _forEach(item) {
        validated = validatePageSettingsOption(pageData.settings, item.id, true);
        result = result || validated.changed;
        input = document.getElementById(makeElementId(item.id, pageData.index));
        if (validated.error) {
          logWarn(addTS(item.name + ': ' + validated.error));
          if (input) {
            input.classList.add('error');
          }
        } else if (input) {
          input.classList.remove('error');
        }
      });
      return result;
    }

    function markOptionAsInvalid(optionId, errorText) {
      var input;

      pageData.settings.invalid = pageData.settings.invalid || {};
      pageData.settings.invalid[optionId] = errorText;
      input = document.getElementById(makeElementId(optionId, pageData.index));
      if (input) {
        input.classList.add('error');
      }
    }

    function nextStep(data) {
      return new window.Promise(function _executor(resolve) {
        setTimeout(function _setTimeout() {
          resolve(data);
        }, smallDelay);
      });
    }

    function createTestDialog(t) {
      var divDialog;

      t = t || {};
      if (pageData.settings.checkUrl) {
        t.url = pageData.settings.checkUrl;
        t.urlId = 'checkUrl';
      } else {
        t.url = pageData.settings.pageUrl;
        t.urlId = 'pageUrl';
      }
      divDialog = createDialogBlock();
      if (!divDialog) {
        throw new Error(strings.errorCreatingTestDialog);
      }
      document.getElementById('dialogTitle').innerText = t.url;
      divDialog.appendChild(createElement('div', 'dialogTestLog', 'dialoglog'));
      showDialog();
      return nextStep(t);
    }

    function prepareRequest(t) {
      addLogText(strings.requestingPageContent + ': ' + t.url, 'info', true);
      return nextStep(t);
    }

    function sendRequest(t) {
      return new window.Promise(function _executor(resolve, reject) {
        var request, aborted, abortTimeout;

        request = new XMLHttpRequest();
        aborted = false;
        request.onreadystatechange = function _onreadystatechange() {
          if (request.readyState === 4) {
            clearTimeout(abortTimeout);
            addLogText(strings.status + ' ' + (aborted ? 0 : request.status) + ': ' + ((aborted ? '' : request.statusText) || strings.networkError), ((!aborted && request.status === 200) ? 'success' : 'error'));
            if (!aborted && request.status === 200 && request.responseText) {
              t.pageText = request.responseText;
              resolve(t);
            } else {
              markOptionAsInvalid(t.urlId, strings.pageContentUnavailable);
              addLogText(strings.failedToLoadPageContent + ': ' + t.url, 'error');
              reject(new Error(strings.failedToLoadPageContent + ': ' + t.url));
            }
          }
        };
        if (typeof request.timeout === 'undefined') {
          abortTimeout = setTimeout(function _setTimeout() { // Safari 5.1 workaround
            aborted = true;
            request.abort();
            logWarn(addTS(strings.log.requestAborted));
          }, 10000);
        } else {
          request.timeout = 10000;
        }
        request.open('GET', t.url);
        request.send();
      });
    }

    function logResponse(t) {
      addLogText(t.pageText);
      addLogText();
      return nextStep(t);
    }

    function matchPageText(t) {
      var match, matchedText;

      addLogText(strings.searchingPageTextFor + ': ' + pageData.settings.find, 'info', true);
      match = /^\/(.+)\/(\w{0,3})$/.exec(pageData.settings.find);
      if (!match) {
        t.regEx = new RegExp(pageData.settings.find);
      } else {
        t.regEx = new RegExp(match[1], match[2]);
      }
      t.match = t.regEx.exec(t.pageText);
      matchedText = '';
      if (t.match && t.match.length > 0) {
        matchedText = t.match[0];
      }
      if (matchedText.length > 0) {
        addLogText(strings.textFoundAtPosition + ' ' + t.match.index + ':', 'success');
      } else {
        addLogText(strings.noMatchesWithRegEx + ': ' + pageData.settings.find, 'error');
        markOptionAsInvalid('find', strings.noMatchesWithThisRegEx);
        throw new Error(strings.noMatchesWithRegEx + ': ' + pageData.settings.find);
      }
      return nextStep(t);
    }

    function logMatchedText(t) {
      var i;

      addLogText(t.pageText.substring(t.match.index - 80, t.match.index), '', false, true, true);
      addLogText(t.match[0], 'highlight', false, true, true);
      addLogText(t.pageText.substring(t.match.index + t.match[0].length, t.match.index + t.match[0].length + 80), '', false, true);
      for (i = 1; i < t.match.length; ++i) {
        addLogText('$' + i + ' = “' + t.match[i] + '”', 'success');
      }
      addLogText();
      return nextStep(t);
    }

    function replacePageText(t) {
      var replace;

      replace = validate('replace', pageData.settings.replace).value;
      if (!replace && t.match.length > 1) {
        replace = Array.apply(null, new Array(t.match.length - 1)).map(function _map(e, i) {
          return '$' + (++i);
        }).join(' ');
      }
      if (replace) {
        addLogText(strings.applyingReplacePattern + ': ' + replace, 'info', true);
        t.match[0] = t.match[0].replace(t.regEx, replace);
        if (t.match[0] === replace) {
          addLogText(strings.incorrectReplacePattern + ' - ' + strings.gotResult + ': ' + t.match[0], 'error');
          markOptionAsInvalid('replace', strings.incorrectReplacePattern);
          throw new Error(strings.incorrectReplacePattern + ' - ' + strings.gotResult + ': ' + t.match[0]);
        }
      }
      if (t.match[0].length > maxHistoryTextLength) {
        addLogText(strings.truncatingResultText, 'info', true);
        t.match[0] = (t.match[0].substring(0, maxHistoryTextLength - 1) + '…');
      }
      addLogText(strings.extractedText + ':', 'success');
      addLogText(t.match[0]);
      pageData.settings.invalid = false;
      return nextStep(t);
    }

    function logTestSuccess() {
      try {
        addLogText();
        addLogText(strings.testPassed, 'success');
      } catch (err) {
        // continue regardless of error
      }
    }

    function logTestError() {
      try {
        addLogText();
        addLogText(strings.testFailed, 'error');
      } catch (err) {
        // continue regardless of error
      }
    }

    function applySettingsChange() {
      updatePageSettingsStatus(pageData.index);
      window.lib.setSettingsStorage();
    }

    pageSettingsChanged = validatePageSettings();
    if (pageData.settings.invalid) {
      if (pageSettingsChanged) {
        applySettingsChange();
      }
      return;
    }
    createTestDialog()
      .then(prepareRequest)
      .then(sendRequest)
      .then(logResponse)
      .then(matchPageText)
      .then(logMatchedText)
      .then(replacePageText)
      .then(logTestSuccess, logTestError)
      .then(applySettingsChange);
  }

  function buildPageBody() {
    var pageIndex, divContainer, divContent, h1Header;

    function createGeneralSetingsBlock() {
      var divGeneral, ulOptions, liOption, checkInterval;

      divGeneral = createElement('div', 'general', 'general');
      ulOptions = createElement('ul', '', 'options');
      checkInterval = validate('checkInterval', window.settingsStorage.checkInterval).value;
      liOption = createOptionItem(strings.labels.checkInterval + ':', 'checkInterval', 'number', {
        type: 'text',
        value: checkInterval
      });
      ulOptions.appendChild(liOption);
      divGeneral.appendChild(ulOptions);
      return divGeneral;
    }

    function createNewPageBlock() {
      var divNewPage, ulOptions;

      divNewPage = createElement('div', 'newPage', 'page');
      ulOptions = createElement('ul', '', 'options');
      ulOptions.appendChild(createOptionItem(strings.labels.newPageUrl + ':', 'newPageUrl', 'text', {
        type: 'text',
        placeholder: strings.placeholders.newPageUrl
      }));
      divNewPage.appendChild(ulOptions);
      return divNewPage;
    }

    function createButtonsBlock() {
      var divButtons;

      divButtons = createElement('div', 'buttons', '');
      divButtons.appendChild(createElement('input', 'btnImportSettings', 'button', {
        type: 'button',
        value: strings.importSettings,
        onclick: handleImportSettings
      }));
      divButtons.appendChild(createElement('input', 'btnExportSettings', 'button', {
        type: 'button',
        value: strings.exportSettings,
        onclick: handleExportSettings
      }));
      divButtons.appendChild(createElement('input', 'fileImportSettings', 'hidden', {
        type: 'file',
        accept: 'application/json',
        onchange: handleImportSettings
      }));
      return divButtons;
    }

    if (!window.settingsStorage) {
      logError(addTS(strings.log.settingsStorageNotDefined));
      return;
    }

    divContainer = document.getElementById('container');

    if (!divContainer) {
      divContainer = createElement('div', 'container', 'container');
      document.body.insertBefore(divContainer, document.body.firstChild);
    }
    clearElement(divContainer);
    h1Header = createElement('h1');
    h1Header.appendChild(createElement('span', '', 'logo'));
    h1Header.appendChild(document.createTextNode(document.title));
    divContainer.appendChild(h1Header);
    divContent = createElement('div', 'content', 'content');
    divContainer.appendChild(divContent);
    divContent.appendChild(createGeneralSetingsBlock());
    divContent.appendChild(createNewPageBlock());
    divContent.appendChild(createButtonsBlock());
    divContainer.appendChild(createElement('div', 'tooltip', 'tooltip'));

    if (isDefined('settingsStorage.pages.length')) {
      for (pageIndex = 0; pageIndex < window.settingsStorage.pages.length; ++pageIndex) {
        addPageSettingsBlock(pageIndex);
      }
    } else {
      logWarn(addTS(strings.log.settingsNoPageSettingsFound));
    }
  }

  function downloadDataUri(filename, data) {
    createElement('a', '', '', { href: data, download: filename }).click();
  }

  function handleShowTooltip(event) {
    var bodyPos, element, elementPos, isButton, tooltip;

    function toPx(number) {
      return String(Math.round(number * 10) / 10) + 'px';
    }

    function getElementText() {
      var item;

      if (isButton) {
        return strings.tooltips[Object.keys(strings.tooltips).find(function _find(className) {
          return element.classList.contains(className);
        })] || '';
      }
      item = parseElementId(element.id);
      if (item && item.settings && item.settings.invalid) {
        return getString(item.settings.invalid[item.id]);
      }
      return '';
    }

    function makeOpaque() {
      if (tooltip.style.display) {
        tooltip.style.opacity = 1;
      }
    }

    element = event.target;
    if (!isElement(element)) {
      return;
    }
    tooltip = document.getElementById('tooltip');
    if (!isElement(tooltip)) {
      return;
    }
    isButton = element.classList.contains('button');
    if (!isButton && !element.classList.contains('error')) {
      return;
    }
    tooltip.innerText = getElementText();
    if (!tooltip.innerText) {
      return;
    }
    bodyPos = document.body.getBoundingClientRect();
    elementPos = element.getBoundingClientRect();
    tooltip.style.display = 'block';
    tooltip.style.top = toPx((elementPos.top - bodyPos.top - tooltip.clientHeight) + (isButton ? 2 : 1));
    tooltip.style.left = toPx(elementPos.left - bodyPos.left - (isButton ? 8 : -8));
    if (!isButton) {
      tooltip.classList.add('error');
    }
    if (!tooltip.timeoutID) {
      tooltip.timeoutID = setTimeout(makeOpaque(), 500);
    }
  }

  function handleHideTooltip() {
    var tooltip;

    tooltip = document.getElementById('tooltip');
    if (!isElement(tooltip)) {
      return;
    }
    if (tooltip.timeoutID) {
      clearTimeout(tooltip.timeoutID);
      tooltip.timeoutID = 0;
    }
    tooltip.classList.remove('error');
    tooltip.style.cssText = ''; // Safari 5.1 workaround
    tooltip.removeAttribute('style');
    tooltip.innerText = '';
  }

  function handleChangeOption(event) {
    var input, option, validated;

    input = event.target;
    if (!isElement(input, 'input')) {
      return;
    }
    option = parseElementId(input.id);
    if (!option) {
      return;
    }
    validated = validate(option.id, input.value);
    input.value = validated.value;
    switch (option.id) {
      case 'newPageUrl':
        if (!validated.error && window.settingsStorage) {
          input.value = '';
          window.settingsStorage.pages = window.settingsStorage.pages || [];
          addPageSettingsBlock(window.settingsStorage.pages.push({
            pageUrl: validated.value
          }) - 1);
        }
        break;
      case 'checkInterval':
        option.settings[option.id] = validated.value;
        input.nextElementSibling.innerText = getTimeIntervalInMinutes(validated.value);
        break;
      default:
        option.settings[option.id] = validated.value;
    }
    if (validated.error) {
      option.settings.invalid = option.settings.invalid || {};
      option.settings.invalid[option.id] = validated.error;
      input.classList.add('error');
      handleShowTooltip({
        target: input
      });
    } else {
      if (option.settings.invalid) {
        delete option.settings.invalid[option.id];
        if (!Object.keys(option.settings.invalid).length) {
          delete option.settings.invalid;
        }
      } else if (option.settings.invalid === false) {
        delete option.settings.invalid;
      }
      input.classList.remove('error');
      handleHideTooltip({
        target: input
      });
    }
    if (typeof option.index !== 'undefined') {
      updatePageSettingsStatus(option.index);
    }
    window.lib.setSettingsStorage();
  }

  function handlePageAction(event) {
    var button, option, divPage;

    button = event.target;
    if (!isElement(button, 'div')) {
      return;
    }
    if (!button.classList.contains('button')) {
      return;
    }
    divPage = button.parentElement.parentElement;
    option = parseElementId(divPage.id);
    if (!option || typeof option.index === 'undefined') {
      return;
    }
    if (button.classList.contains('delete')) {
      window.settingsStorage.pages[option.index] = undefined;
      divPage.parentElement.removeChild(divPage);
      window.lib.setSettingsStorage();
    } else {
      runPageSettingsTest(option);
    }
  }

  function handleMessage(event) {
    switch (event.name) {
      case 'getSettingsStorage':
        if (!window.settingsStorage) {
          window.settingsStorage = cleanUpSettings(event.message);
          setLogging();
          buildPageBody();
          logInfo(addTS(strings.log.pageInitializationCompleted));
        }
        logInfo(addTS(strings.log.handledMessage + ': ' + event.name));
        break;
      case 'getCheckHistory':
        window.checkHistory = event.message;
        logInfo(addTS(strings.log.handledMessage + ': ' + event.name));
        break;
      default:
        logWarn(addTS(strings.log.unhandledMessage + ': ' + event.name));
    }
  }

  function handleExportSettings(event) {
    var data, url, exportHistory, showJsonData;

    if (isDefined('settingsStorage.pages.length')) {
      showJsonData = event.altKey;
      exportHistory = event.metaKey;
      if (exportHistory) {
        window.lib.getCheckHistory();
      }
      setTimeout(function _setTimeout() {
        data = '\ufeff' + JSON.stringify({
          id: 'dev.eip.updatechecker',
          version: '0.5',
          settings: cleanUpSettings(window.settingsStorage),
          history: exportHistory ? window.checkHistory : undefined
        }, null, showJsonData ? '  ' : undefined);
        url = 'data:application/json;charset=utf-8;base64,' + b64EncodeUnicode(data);
        if (showJsonData) {
          window.open(url, '_blank');
        } else {
          downloadDataUri(addFileNameTS('update-checker-settings' + (exportHistory ? '-and-history' : ''), 'json'), url);
        }
        window.checkHistory = undefined;
      }, exportHistory ? 500 : 1);
    }
  }

  function handleImportSettings(event) {
    //
    function processImportText(value) {
      var importObj, validPageUrl, i, newPage;

      try {
        importObj = JSON.parse(value);
        if (importObj && importObj.id === 'dev.eip.updatechecker' && parseFloat(importObj.version) >= 0.5 && importObj.settings) {
          window.settingsStorage = cleanUpSettings(window.settingsStorage);
          Object.keys(importObj.settings).filter(function _filter(option) {
            return option !== 'pages';
          }).forEach(function _forEach(option) {
            window.settingsStorage[option] = importObj.settings[option];
          });
          importObj.settings.pages.forEach(function _forEach(page) {
            validPageUrl = validate('pageUrl', page.pageUrl);
            if (validPageUrl.error) {
              logWarn(addTS(validPageUrl.error + ': "' + page.pageUrl + '"'));
              return;
            }
            page.pageUrl = validPageUrl.value;
            newPage = true;
            for (i = 0; i < window.settingsStorage.pages.length; ++i) {
              if (window.settingsStorage.pages[i].pageUrl === page.pageUrl) {
                window.settingsStorage.pages[i] = page;
                newPage = false;
                break;
              }
            }
            if (newPage) {
              window.settingsStorage.pages.push(page);
            }
          });
          window.lib.setSettingsStorage();
          buildPageBody();
        } else {
          logError(addTS(strings.log.importSettingsError));
        }
      } catch (err) {
        logError(addTS(strings.log.importSettingsError + ': ' + err.message));
      }
    }

    function importFromFile(file) {
      var reader;

      if (!file) {
        return;
      }
      reader = new window.FileReader();
      reader.onload = function _onload(e) {
        processImportText(e.target.result);
      };
      reader.readAsText(file);
    }

    function importViaDialog() {
      var divDialog;

      function createImportDialogContent() {
        var divDialogContent, divButtons;

        divDialogContent = createElement('div', 'dialogContent', 'dialogcontent');
        divDialog.appendChild(divDialogContent);
        if (!divDialogContent) {
          return;
        }
        divDialogContent.appendChild(createElement('textarea', 'importText', '', {
          cols: 80,
          rows: 25,
          placeholder: strings.pasteExportedSettingsTextHere
        }));
        divButtons = createElement('div', 'dialogButtons', '');
        divButtons.appendChild(createElement('input', 'btnImportImport', 'button', {
          type: 'button',
          value: strings.importLbl,
          onclick: function _onclick() {
            var e;

            e = document.getElementById('importText');
            if (e && e.value.length > 10) {
              processImportText(e.value);
            }
            closeDialog();
          }
        }));
        divButtons.appendChild(createElement('input', 'btnImportCancel', 'button', {
          type: 'button',
          value: strings.cancelLbl,
          onclick: closeDialog
        }));
        divDialogContent.appendChild(divButtons);
      }

      divDialog = createDialogBlock();
      if (!divDialog) {
        throw new Error(strings.errorCreatingImportDialog);
      }
      createImportDialogContent();
      document.getElementById('dialogTitle').innerText = strings.importSettings; // TODO
      showDialog();
      document.getElementById('importText').focus();
    }

    if (event.type === 'click' && event.target && event.target.id === 'btnImportSettings') {
      if (window.FileReader) {
        document.getElementById('fileImportSettings').click();
      } else {
        importViaDialog();
      }
    } else if (event.type === 'change' && event.target && event.target.id === 'fileImportSettings') {
      importFromFile(event.target.files[0]);
      event.target.value = '';
    }
  }

  function initPublicFunctions() {
    window.lib = window.lib || {};

    window.lib.getSettingsStorage = function _getSettingsStorage() {
      safari.self.tab.dispatchMessage('getSettingsStorage');
    };

    window.lib.setSettingsStorage = function _setSettingsStorage() {
      safari.self.tab.dispatchMessage('setSettingsStorage', cleanUpSettings(window.settingsStorage));
    };

    window.lib.getCheckHistory = function _getCheckHistory() {
      safari.self.tab.dispatchMessage('getCheckHistory');
    };

    window.lib.setCheckHistory = function _setCheckHistory() {
      safari.self.tab.dispatchMessage('setCheckHistory', window.checkHistory);
    };
  }

  function initEventListeners() {
    safari.self.addEventListener('message', handleMessage, false);
  }

  setConstants();
  setLogging();
  initPublicFunctions();
  initEventListeners();
  window.lib.getSettingsStorage();
}());
