# Update Checker

A Safari extension that automatically checks web pages for updates.

### Installation

[Download](https://bitbucket.org/eip/update-checker/downloads) the extension and open it with Safari 5.1 or later.

### Usage

You need basic knowledge of [regular expressions](https://en.wikipedia.org/wiki/Regular_expression) to configure this extension.

Open Safari’s preferences using `CMD + ,` or from the Safari menu. Go to the Extensions tab, select Update Checker and click the checkbox labeled "Click me to show the settings window".

Update Checker Settings page will be opened. See the example page settings and add some pages for checking.

Click the toolbar button to see the history of pages' content changes.
